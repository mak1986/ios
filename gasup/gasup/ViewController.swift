//
//  ViewController.swift
//  gasup
//
//  Created by iOS Dev on 9/10/2558 BE.
//  Copyright (c) 2558 iOS Dev. All rights reserved.
//

import UIKit
import SwiftHTTP
import Kanna

class ViewController: UIViewController {
    var allPrices:Dictionary<String, Dictionary<String,Double!>> = Dictionary<String, Dictionary<String,Double!>>()
    var companies:Array<Company> = Array<Company>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //self.companies.append(Ptt())
        //self.companies.append(Caltex())
        //self.companies.append(Bangchak())
        for company in companies{
            company.reload(updatePrices)
        }
        //print(caltex.getPrices())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func updatePrices(company:String, prices:Dictionary<String,Double!> ){
        allPrices[company]=prices
        print(allPrices)
    }
}

