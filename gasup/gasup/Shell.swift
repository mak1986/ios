//
//  Shell.swift
//  gasup
//
//  Created by Jacobsen on 12/12/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import Foundation
import SwiftHTTP
import Kanna

public class Shell: Company{
    private var url:String! = "http://www.shell.co.th/th/products-services/on-the-road/fuels/fuel-price/app-fuel-prices.html"
    private var prices:Dictionary<String,Double!> = [
        "Gasohol 95-E10":nil,
        "Gasohol 95-E20":nil,
        "Gasohol 95-E85":nil,
        "Gasohol 91-E10":nil,
        "Benzine 95":nil,
        "Diesel":nil,
        "Diesel Premium":nil,
        "NGV":nil
    ]
    public init(){}
    
    public func reload(onSuccess: (companyName:String, prices:Dictionary<String, Double!>)->Void){
        var downloadedPrices:[Double] = [Double]()
        do{
            let opt = try HTTP.GET(self.url)
            opt.start { response in
                if let err = response.error{
                    print("error: \(err.localizedDescription)")
                    return
                }
                let doc = Kanna.HTML(html: response.text!, encoding: NSUTF8StringEncoding)!
                var i = 0
                for item in doc.css("table.grey_global_table tr"){
                    
                    if( i >= 0 && i < 5 ){

                        let temp1 = item.text!.stringByReplacingOccurrencesOfString("\n", withString: "")
                        let temp2:String = temp1.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                        downloadedPrices.append(Double(temp2.substringFromIndex(temp2.endIndex.predecessor().predecessor().predecessor().predecessor().predecessor()))!)
                    }
                    i++
                }
                
                self.prices["Diesel"] = downloadedPrices[0]
                self.prices["Diesel Premium"] = downloadedPrices[1]
                self.prices["Gasohol 95-E20"] = downloadedPrices[2]
                self.prices["Gasohol 91-E10"] = downloadedPrices[3]
                self.prices["Benzine 95"] = downloadedPrices[4]
                
                
                onSuccess(companyName: "Shell", prices: self.prices)
            }
        } catch{}
    }
    public func getPrices() -> Dictionary<String, Double!> {
        return self.prices
    }
}
