//
//  MapViewController.swift
//  gasup
//
//  Created by Jacobsen on 12/11/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class MapViewController: UIViewController, CLLocationManagerDelegate  {

    @IBOutlet weak var mapView: MKMapView!
    let regionRadius: CLLocationDistance = 2000
    let locationManager = CLLocationManager()
    var currentLocation:CLLocation!
    var ready = false
    var searchString:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Ask for Authorisation from the User.
        //self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            
        }
        // Do any additional setup after loading the view.
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.currentLocation = manager.location
        if(ready==false){
            centerMapOnLocation(manager.location!)
            
            ready = true

            let request = MKLocalSearchRequest()
            request.naturalLanguageQuery = "gas station "+searchString!
            //print(searchString)
            request.region = mapView.region
            
            let search = MKLocalSearch(request: request)
            search.startWithCompletionHandler { (response, error) in
                guard let response = response else {
                    //print("Search error: \(error)")
                    return
                }
                
                for item in response.mapItems {
                    //print(item.name)
                    //print(item.placemark.coordinate)
                    let tankStation = TankStation(title: item.name!, coordinate: item.placemark.coordinate)
                    self.mapView.addAnnotation(tankStation)
                }
            }
            
            
            
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
        regionRadius * 2.0, regionRadius * 2.0)
    mapView.setRegion(coordinateRegion, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
