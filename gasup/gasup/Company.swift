//
//  Company.swift
//  gasup
//
//  Created by Jacobsen on 12/10/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import Foundation

protocol Company{
    func reload(onSuccess: (companyName: String, prices:Dictionary<String, Double!>)->Void)
    func getPrices() -> Dictionary<String, Double!>
}