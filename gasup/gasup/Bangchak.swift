//
//  Bangchak.swift
//  gasup
//
//  Created by Jacobsen on 12/10/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import Foundation
import SwiftHTTP
import Kanna

public class Bangchak: Company{
    private var url:String! = "http://www.bangchak.co.th/th/oil-price-history.aspx"
    private var prices:Dictionary<String,Double!> = [
        "Gasohol 95-E10":nil,
        "Gasohol 95-E20":nil,
        "Gasohol 95-E85":nil,
        "Gasohol 91-E10":nil,
        "Benzine 95":nil,
        "Diesel":nil,
        "Diesel Premium":nil,
        "NGV":nil
    ]
    public init(){}
    
    public func reload(onSuccess: (companyName:String, prices:Dictionary<String, Double!>)->Void){
        var downloadedPrices:[Double] = [Double]()
        do{
            let opt = try HTTP.GET(self.url)
            opt.start { response in
                if let err = response.error{
                    print("error: \(err.localizedDescription)")
                    return
                }
                let doc = Kanna.HTML(html: response.text!, encoding: NSUTF8StringEncoding)!
                var i = 0
                for item in doc.css("table#dlOilPrice td"){
                    if( i > 11 && i < 18 ){
                        downloadedPrices.append(Double(item.text!)!)
                    }
                    i++
                }
                self.prices["Gasohol 95-E10"] = downloadedPrices[0]
                self.prices["Gasohol 91-E10"] = downloadedPrices[1]
                self.prices["Gasohol 95-E20"] = downloadedPrices[2]
                self.prices["Gasohol 95-E85"] = downloadedPrices[3]
                self.prices["Diesel"] = downloadedPrices[4]
                self.prices["NGV"] = downloadedPrices[5]
                
                onSuccess(companyName: "Bangchak", prices: self.prices)
            }
        } catch{}
    }
    public func getPrices() -> Dictionary<String, Double!> {
        return self.prices
    }
}
