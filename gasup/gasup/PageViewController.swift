//
//  PageViewController.swift
//  gasup
//
//  Created by Jacobsen on 12/10/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController,UIPageViewControllerDelegate, UIPageViewControllerDataSource{

    let proxy = UIPageControl.appearance()
    var startingIndex = 0
    var currentIndex = 0
    var allPrices:Dictionary<String, Dictionary<String,Double>> = Dictionary<String, Dictionary<String,Double>>()
    var companies:Array<Company> = Array<Company>()
    var fuelTypes:Array<String> = [
        "Gasohol 95-E10",
        "Gasohol 95-E20",
        "Gasohol 95-E85",
        "Gasohol 91-E10",
        "Benzine 95",
        "Diesel",
        "Diesel Premium",
        "NGV"
    ]
    var priceViewControllers:[PriceViewController] = [PriceViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        
        initPageViewControllers()

        self.companies.append(Ptt())
        self.companies.append(Caltex())
        self.companies.append(Bangchak())
        self.companies.append(Shell())
        self.companies.append(Esso())
        for company in companies{
            company.reload(updatePrices)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updatePrices(company:String, prices:Dictionary<String,Double!> ){
        //allPrices[company]=prices
        
        for fuelType in prices.keys{
            if (allPrices[fuelType] == nil){
                allPrices[fuelType] = Dictionary<String,Double>()
            }
            let price = prices[fuelType]!
            if price != nil{
                allPrices[fuelType]![company] = price
                //print(allPrices[fuelType]![company])
            }
        }
        //print("\n")
        for key in allPrices.keys{
            let index = fuelTypes.indexOf(key)
            let prices = allPrices[key]!.sort({ (e1, e2) -> Bool in
                return e1.1 < e2.1
            })
            priceViewControllers[index!].setPrices(prices)
        }
        //priceViewControllers[in]
        //print(allPrices)
    }
    
    func initPageViewControllers(){
        
        for(var i = 0; i < fuelTypes.count; i++){
            self.priceViewControllers.append(createControllerAtIndex(i))
        }
        self.setViewControllers([self.priceViewControllers[0]], direction: .Forward, animated: false, completion: nil)
            

    }
    
    func createControllerAtIndex(index:Int)->PriceViewController!{
        let controller:PriceViewController = storyboard!.instantiateViewControllerWithIdentifier("PriceViewController") as! PriceViewController
        controller.fuelType = fuelTypes[index]
        controller.pageIndex = index
        return controller
    }

    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! PriceViewController
        let index = vc.pageIndex
        if (index < fuelTypes.count-1){
            return self.priceViewControllers[index+1]
        }else{
            return nil
        }
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! PriceViewController
        let index = vc.pageIndex
        if index > 0{
            return self.priceViewControllers[index-1]
        }else{
            return nil
        }
    }
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return fuelTypes.count
    }
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return startingIndex
    }
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        self.currentIndex = priceViewControllers[0].pageIndex
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
