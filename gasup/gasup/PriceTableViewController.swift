//
//  PriceTableViewController.swift
//  gasup
//
//  Created by Jacobsen on 12/11/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit

class PriceTableViewController: UITableViewController {
    var db = Database.getSharedInstance()
    var prices:[(String,Double)] = [(String, Double)]()
    var enablePrices:[(String, Double)] = [(String, Double)]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setEnablePrices()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        
        //spinner.startAnimating()
    }
    func setEnablePrices(){
        let companies = db.getCompanies()
        enablePrices = [(String,Double)]()
        for company in companies{
            if (company.enable == true && keyExists(company.name)) {
                self.enablePrices.append((company.name,getValueByKey(company.name)))
            }
        }
    }
    func keyExists(key:String)->Bool{
        for i in prices{
            if i.0 == key{
                return true
            }
        }
        return false
    }
    func getValueByKey(key:String)->Double!{
        for i in prices{
            if i.0 == key{
                return i.1
            }
        }
        return nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return enablePrices.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("priceCell", forIndexPath: indexPath)
        cell.textLabel?.text = enablePrices[indexPath.item].0
        cell.detailTextLabel?.text = String(format:"%.2f",Double(enablePrices[indexPath.item].1))
        return cell

    }
    
    func setPrices(prices:[(String,Double)]){
        self.prices = prices
        self.setEnablePrices()
        self.tableView.reloadData()
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "toMapViewControllerSegue"){
            let destination = segue.destinationViewController as! MapViewController
            let cell = sender as! UITableViewCell
            destination.searchString = cell.textLabel?.text
        }
    }
    

}
