//
//  TankStation.swift
//  gasup
//
//  Created by Jacobsen on 12/11/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import MapKit
import UIKit

public class TankStation: NSObject, MKAnnotation {
    public var title: String?
    public var coordinate: CLLocationCoordinate2D
    
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
}