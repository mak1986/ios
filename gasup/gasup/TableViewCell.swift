//
//  TableViewCell.swift
//  gasup
//
//  Created by Jacobsen on 12/10/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    var db = Database.getSharedInstance()
    var companyModel:CompanyModel!
    
    @IBOutlet weak var settingSwitch: UISwitch!
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func switchTab(sender: UISwitch) {
        self.companyModel = self.db.setEnable(self.companyModel, state: sender.on)
        
    }
    func setup(index: Int){
        self.companyModel = db.getCompanyModel(index)
        self.label?.text = companyModel.name
        self.settingSwitch.setOn(self.companyModel.enable, animated: false)
    }

}
