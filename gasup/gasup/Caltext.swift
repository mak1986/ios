//
//  Caltext.swift
//  gasup
//
//  Created by Jacobsen on 12/9/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import Foundation
import SwiftHTTP
import Kanna

public class Caltext: Company{
    private var url:String! = "http://www.caltex.com/th"
    
    private init(){}
    
    public func getPrices()->String{
        var result = ""
        do{
            let opt = try HTTP.GET(self.url)
            opt.start { response in
                if let err = response.error{
                    print("error: \(err.localizedDescription)")
                    return
                }
                //                print("opt finish: \(response.description)")
                //print("data is: \(response.text!)")
                let doc = Kanna.HTML(html: response.text!, encoding: NSUTF8StringEncoding)!
                result = doc.at_css("table")!.at_css("table")!.text!
                
            }
        } catch let error{
            print("got an error creating the request: \(error)")
        }
        return result
    }
}
