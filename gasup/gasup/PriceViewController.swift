//
//  PriceViewController.swift
//  gasup
//
//  Created by Jacobsen on 12/10/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit
import SwiftHTTP
import Kanna
class PriceViewController: UIViewController {
    @IBOutlet weak var updatedAtLabel: UILabel!

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var priceHeader: UILabel!
    var pageIndex:Int!
    
    var prices:[(String,Double)] = [(String,Double)]()
    
    var fuelType:String = ""
    
    var priceTableViewController:PriceTableViewController!
    
    @IBOutlet weak var label: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        label.text = fuelType
        if fuelType == "NGV"{
            priceHeader.text = "Unit Price Baht/Kg"
        }else{
            priceHeader.text = "Unit Price Baht/Litre"
        }
        label.layer.masksToBounds = true

        label.layer.cornerRadius = 15.0;
        setUpdatedAt()
    }
    func setUpdatedAt(){
        let timestamp = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
        
        self.updatedAtLabel?.text = "Updated at: " + timestamp
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPrices(prices:[(String,Double)]){
        self.prices = prices
        self.priceTableViewController?.setPrices(prices)
        self.reloadInputViews()
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "embedPriceTableViewControllerSegue"){
            let destination = segue.destinationViewController as! PriceTableViewController
            destination.setPrices(self.prices)
            //print(self.fuelType)
            //print(self.prices)
            self.priceTableViewController = destination
        }
    }
//    func refresh(){
//        self.priceTableViewController.setPrices(self.prices)
//    }
}
