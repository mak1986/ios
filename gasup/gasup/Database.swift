//
//  Database.swift
//  gasup
//
//  Created by Jacobsen on 12/11/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import Foundation

class Database {
    
    private var databasePathString:String! = nil
    
    private var database:COpaquePointer = nil
    
    struct property {
        
        static var sharedInstance:Database! = nil
        
    }
    
    private init(){}
    
    static private func createDB() -> Bool{
        
        let db = Database()
        
        var isSuccess:Bool = true;
        
        let fm = NSFileManager.defaultManager()
        
        //Get Documents url
        
        do {
            
            let docsurl = try fm.URLForDirectory(NSSearchPathDirectory.DocumentDirectory,inDomain: NSSearchPathDomainMask.UserDomainMask,appropriateForURL: nil, create: true)
            
            // Creat folder

            let myFolderUrl = docsurl.URLByAppendingPathComponent("Database")

            try fm.createDirectoryAtURL(myFolderUrl,withIntermediateDirectories: true,attributes: nil)
            
            // Create path to database file
            
            let myDbFileUrl = myFolderUrl.URLByAppendingPathComponent("gasup.db")
            db.databasePathString = myDbFileUrl.path
            print(db.databasePathString)
            if(!fm.fileExistsAtPath(db.databasePathString)){
                
                // Db File not exists. Lets make one
                
                if(sqlite3_open(db.databasePathString, &db.database) == SQLITE_OK){
                    var errMSG:UnsafeMutablePointer<Int8> = nil
                    myFolderUrl.URLByAppendingPathComponent("gasup.db")
                    
                    let text = "CREATE TABLE \"company\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , \"name\"TEXT, \"enable\" BOOL)"
                    
                    let sql_stmt = text.cStringUsingEncoding(NSUTF8StringEncoding);
                    
                    if(sqlite3_exec(db.database, sql_stmt!, nil, nil, &errMSG) != SQLITE_OK){
                        isSuccess = false
                        NSLog("Failed to create memo table")
                    }
                    
                    
                    let sql_insert_stmt = "INSERT INTO company (name , enable) VALUES (\"Ptt\",\"1\");INSERT INTO company (name , enable) VALUES (\"Caltex\",\"1\");INSERT INTO company (name , enable) VALUES (\"Bangchak\",\"1\");INSERT INTO company (name , enable) VALUES (\"Shell\",\"1\");INSERT INTO company (name , enable) VALUES (\"Esso\",\"1\");".cStringUsingEncoding(NSUTF8StringEncoding)
                    
                    var errMsg:UnsafeMutablePointer<Int8> = nil
                    sqlite3_exec(db.database, sql_insert_stmt!, nil, nil, &errMsg)
                    
                    
                    sqlite3_close(db.database);
                    
                    // Store shared when successful
                    
                    property.sharedInstance = db;
                }else{
                    isSuccess = false
                    
                    NSLog("Failed to open/create database");
                }
            }else{
                
                property.sharedInstance = db;
                
            }
        }catch{
            
        }
        
        return isSuccess
    }
    
    static func getSharedInstance() -> Database{
        
        if (property.sharedInstance == nil) {
            
            createDB();
            
        }
        
        return property.sharedInstance;
        
    }

    
    func countCompany() -> Int{
        
        var recordNumber:Int = -1;
        
        var statement:COpaquePointer = nil;
        
        if (sqlite3_open(self.databasePathString, &self.database) == SQLITE_OK){
            
            // Get the latest record number
            
            let lookupSQL = "SELECT count(id) FROM company"
            
            let sql_stmt = lookupSQL.cStringUsingEncoding(NSUTF8StringEncoding)
            
            sqlite3_prepare_v2(self.database, sql_stmt!,-1, &statement, nil)
            
            if (sqlite3_step(statement) == SQLITE_ROW){
                
                recordNumber = Int(sqlite3_column_int(statement, 0));
                
            }
            
            sqlite3_finalize(statement);
            
            sqlite3_close(database);
            
        }
        
        return recordNumber;
        
    }
    
    
    func getCompanyModel(recordID:Int) -> CompanyModel{
        
        let result:CompanyModel = CompanyModel();
        
        var statement:COpaquePointer = nil;
        
        if (sqlite3_open(self.databasePathString, &self.database) == SQLITE_OK){
            
            // Get the latest record number
            
            let lookupSQL = "SELECT name, enable FROM company WHERE id = \(recordID)"
            
            let sql_stmt = lookupSQL.cStringUsingEncoding(NSUTF8StringEncoding)
            sqlite3_prepare_v2(self.database, sql_stmt!,-1,&statement, nil)
            
            sqlite3_step(statement)
            result.id = recordID
            result.name = String.fromCString(UnsafePointer<CChar>(sqlite3_column_text(statement, 0)))!
            let enableTemp = String.fromCString(UnsafePointer<CChar>(sqlite3_column_text(statement, 1)))!
            if(enableTemp == "1"){
                result.enable = true
            }else{
                result.enable = false
            }
            
            
            sqlite3_finalize(statement);
            
            sqlite3_close(database);
            
        }
        
        return result;
    }
    func setEnable(companyModel:CompanyModel, state:Bool)->CompanyModel{
        
        var statement:COpaquePointer = nil;
        
        if (sqlite3_open(self.databasePathString, &self.database) == SQLITE_OK){
            
            // Get the latest record number
            var updateSQL:String!
            if(state){
                updateSQL = "UPDATE company SET enable = 1 WHERE id = \(companyModel.id)"
                companyModel.enable = true
            }else{
                updateSQL = "UPDATE company SET enable = 0 WHERE id = \(companyModel.id)"
                companyModel.enable = false
            }
            
            let sql_stmt = updateSQL.cStringUsingEncoding(NSUTF8StringEncoding)
            sqlite3_prepare_v2(self.database, sql_stmt!,-1,&statement, nil)
            
            sqlite3_step(statement)
            
            sqlite3_finalize(statement);
            
            sqlite3_close(database);
            
        }
        return companyModel
    }
    func getCompanies()->[CompanyModel]{
        var companyModels:[CompanyModel] = [CompanyModel]();
        
        var statement:COpaquePointer = nil;
        
        if (sqlite3_open(self.databasePathString, &self.database) == SQLITE_OK){
            
            // Get the latest record number
            
            let lookupSQL = "SELECT id, name, enable FROM company"
            
            let sql_stmt = lookupSQL.cStringUsingEncoding(NSUTF8StringEncoding)
            sqlite3_prepare_v2(self.database, sql_stmt!,-1,&statement, nil)
            while(true){
                var rc = sqlite3_step(statement)
                if(rc == SQLITE_DONE){
                    break
                }
                let companyModel = CompanyModel()
                
                companyModel.id = Int(String.fromCString(UnsafePointer<CChar>(sqlite3_column_text(statement, 0)))!)
                companyModel.name = String.fromCString(UnsafePointer<CChar>(sqlite3_column_text(statement, 1)))!
                let enableTemp = String.fromCString(UnsafePointer<CChar>(sqlite3_column_text(statement, 2)))!
                if(enableTemp == "1"){
                    companyModel.enable = true
                }else{
                    companyModel.enable = false
                }
                companyModels.append(companyModel)

                
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
            
        }
        
        return companyModels
    }
}

class CompanyModel{
    var id:Int!
    var name:String = ""
    var enable:Bool!
}